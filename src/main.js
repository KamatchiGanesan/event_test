import Vue from "vue";
import App from "./App";

// BootstrapVue add
import BootstrapVue from "bootstrap-vue";
// Router & Store add
import router from "./router";
import store from "./store";
// Session add
import VueSession from "vue-session";
Vue.use(BootstrapVue);
var options = {
  persist: true,
};
Vue.use(VueSession, options);

Vue.config.productionTip = false;

export default new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
