import Vue from "vue";
import Router from "vue-router";
// const home = () => import("@/pages/Home.vue");
const login = () => import("./components/Login.vue");
// const event_create = () => import("./components/Events/CreateEvent.vue");
const api_list = () => import("./components/Events/apiList.vue");
const form = () => import("./components/VulidateForm/create_form");


const router = new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "login",
      component: login,
    },
    {
      path: "/home",
      name: "form",
      component: form,
    },
    {
      path: "/api-list",
      name: "api list",
      component: api_list,
    },
    // {
    //   path: "/create-events",
    //   name: "event create",
    //   component: event_create,
    // },
  ],
});
router.beforeEach((to, from, next) => {
  // redirect to login page if not logged in and trying to access a restricted page
  const publicPages = ["/", "/events"];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = () => this.$session.get("token");
  if (authRequired && !loggedIn) {
    return next("/");
  }
  next();
});

export default router;
Vue.use(Router);
