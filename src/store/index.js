import Vue from 'vue'
import Vuex from 'vuex'


Vue.use(Vuex)

export default new Vuex.Store({
  state: {
      formData:[]
  },
  getters:{
    getFormData(state){
        return state.formData
    }
  },
  mutations: {
    changeData (state, payload) {
        state.formData.push(payload)
    }
  },
  actions: {
    changeFormData({ commit }, payload) {
      commit('changeData', payload)
    }
  }

})
