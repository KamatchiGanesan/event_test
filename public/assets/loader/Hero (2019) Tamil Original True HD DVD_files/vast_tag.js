// append iframe for tracking
(function(){
    var i = document.createElement('iframe');
    i.style.display = 'none';
    i.id = 'mMFrameRef';
    i.onload = function() {
        //i.parentNode.removeChild(i);
        mMCommunicate();
    };
    i.src = 'https://mahimeta.com/networks/frame_ref.php';
    document.body.appendChild(i);
})();

function mMCommunicate(){

    domain = document.domain;
    user_domain = domain;
	user_path = window.location.pathname;
	user_query = window.location.search;

	user_fullpath = user_domain + user_path + user_query;

    var iframeWin = document.getElementById("mMFrameRef").contentWindow;
    iframeWin.postMessage(user_fullpath, "*");    

}

// listener
var eventMethodMahimeta = window.addEventListener
        ? "addEventListener"
        : "attachEvent";
var eventer = window[eventMethodMahimeta];
var messageEvent = eventMethodMahimeta === "attachEvent"
    ? "onmessage"
    : "message";
eventer(messageEvent, function (e) {

    if( e.data == undefined ){
        e.data = e.message;
    }

});
